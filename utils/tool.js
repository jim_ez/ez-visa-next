import axios from "axios";
import EE from "exenv";

const canUseDOM = EE.canUseDOM;

export const fetchApi = async uri => {
  let res;
  await axios
    .get(uri)
    .then(function(response) {
      res = response.data;
    })
    .catch(err => {
      console.log("err: ", err);
      res = null;
    });
  return res;
};

export const clearScriptTag = html => {
  let srcRegEx = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
  return html.replace(srcRegEx, "");
};

export const getScriptTag = html => {
  let srcRegEx = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
  if (html.match(srcRegEx)) {
    return html
      .match(srcRegEx)
      .toString()
      .replace(",", "");
  } else {
    return null;
  }
};

/**
 * 取 cookie
 * @param {string} name - cookie name
 */
export const getCookieByName = (name, cookieStr) => {
  let value = "";
  const cookieResource = cookieStr || (canUseDOM && document.cookie) || "";
  const cookieWithPrefix = "; " + cookieResource;
  const cookieParts = cookieWithPrefix.split(`; ${name}=`);
  if (cookieParts.length == 2) {
    value = cookieParts
      .pop()
      .split(";")
      .shift();
  }
  return value;
};
