const next = require("next");
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const port = parseInt(process.env.PORT, 10) || 3000;

const Koa = require("koa");
const serve = require("koa-static");
const Router = require("koa-router");
const mount = require("koa-mount");
const bodyParser = require("koa-bodyparser");

const visaControllers = require("./server/controllers/visaController");

const env = process.env.NODE_ENV;
const open = require("open");

app.prepare().then(() => {
  const server = new Koa();
  const router = new Router();

  //能接到post request body (一定要放最前面!)
  server.use(bodyParser());

  //slbcheck.htm 檢查網路連線用 (gitignore)
  server.use(mount("/assets", serve(__dirname + "/assets")));

  if (dev) {
    // 本機從 /build/images 取得 webpack build 完的 image
    server.use(mount(`/static/images`, serve(__dirname + "/build/images")));
  }

  server.use(router.routes());

  router.get("/about", async (ctx) => {
    ctx.status = 200;
    const query = {
      ...ctx.params,
      ...ctx.query,
      ezHeaderFooter: "simple",
    };
    await app.render(ctx.req, ctx.res, "/about", query);
    ctx.respond = false;
  });

  //API
  router.get("/api/getVisaFree", visaControllers.getVisaFree);
  router.get("/api/getVisaOnArrival", visaControllers.getVisaOnArrival);
  router.get("/api/getPassport", visaControllers.getPassport);
  router.get("/api/getVisaCountry", visaControllers.getVisaCountry);
  router.post("/api/getVisaList", visaControllers.getVisaList);
  router.post("/api/getVisaDetail", visaControllers.getVisaDetail);

  router.get("*", async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
    // 開發區時，自動開啟預設瀏覽器
    if (env == "dev") {
      open("http://3w.eztravel.com.tw/");
    }
  });
});
