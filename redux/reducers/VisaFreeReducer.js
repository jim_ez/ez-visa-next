import * as actionTypes from "../constants";

export const INITIAL_STATE = {
  loading: true,
  lists: [],
  selected: false,
};

function Reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case actionTypes.FETCH_VISAFREE:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.FETCH_VISAFREE_SUCCESS:
      return {
        ...state,
        loading: false,
        lists: action.data,
      };
    case actionTypes.FETCH_VISAFREE_FAIL:
      return {
        ...state,
        loading: false,
      };
    case actionTypes.SHOW_VISAFREE_DETAIL:
      return {
        ...state,
        selected: action.no,
      };
  }
  return state;
}

export default Reducer;
