import React from "react";
import PropTypes from "prop-types";
import styles from "./Bread.scss";

const Bread = ({ step }) => {
  return (
    <div className={styles.step}>
      <div className={styles.bread}>
        <div className={step == 1 ? styles.active : ""}>訂單明細</div>
        <div className={step == 2 ? styles.active : ""}>填寫訂購單</div>
        <div className={step == 3 ? styles.active : ""}>選擇付款方式</div>
        <div className={step == 4 ? styles.active : ""}>完成訂購</div>
      </div>
    </div>
  );
};

Bread.propTypes = {
  step: PropTypes.number
};

Bread.defaultProps = {
  step: ""
};

export default Bread;
