import React from "react";
import * as actionTypes from "../constants";
import ContextStore from "../contexts/orderContext";

function OrderList() {
  const { orders, prods, oDispatch, pDispatch } = React.useContext(
    ContextStore
  );
  return (
    <>
      {orders.map((orders, index) => (
        <div key={`ordersList${index}`}>Order - {orders.id}</div>
      ))}
      <button
        onClick={() =>
          oDispatch({
            type: actionTypes.ADD_LIST,
            data: { id: "order4" }
          })
        }
      >
        ADD ORDER
      </button>
      {prods.map((prods, index) => (
        <div key={`prodsList${index}`}>Product - {prods.id}</div>
      ))}
      <button
        onClick={() =>
          pDispatch({
            type: actionTypes.ADD_LIST,
            data: { id: "prod4" }
          })
        }
      >
        ADD PRODUCT
      </button>
    </>
  );
}

export default OrderList;
