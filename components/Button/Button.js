import React from "react";
import PropTypes from "prop-types";
import styles from "./Button.scss";

const Button = ({
  id,
  text,
  color,
  type,
  onClick,
  style,
  loading,
  disabled
}) => {
  const classNameArr = [styles[color]];
  classNameArr.push(styles.button);
  if (loading) classNameArr.push(styles.loading);
  if (disabled) classNameArr.push(styles.disabled);
  const className = classNameArr.join(" ");

  return (
    <button
      id={id}
      type={type}
      onClick={loading ? null : onClick}
      className={className}
      style={style}
      disabled={disabled ? disabled : loading}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  id: PropTypes.string,
  /**
   * 按鈕文字
   * */
  text: PropTypes.string,
  /**
   * 顏色
   */
  color: PropTypes.oneOf(["green", "gray", "white"]),
  type: PropTypes.oneOf(["button", "reset", "submit"]),
  /**
   * 是否顯示 loading 中
   * */
  loading: PropTypes.bool,
  /**
   * 點擊事件
   * */
  onClick: PropTypes.func
};

Button.defaultProps = {
  id: "",
  text: "",
  type: "button",
  color: "green",
  loading: false
};

export default Button;
