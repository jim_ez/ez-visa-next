/**
 * 全域變數只能在後端使用
 * 不會被bundle進瀏覽器載入的JS檔
 * 更新檔案需重啟server
 */
const os = require("os");
const hostname = os.hostname();

let hostTag = process.env.API_ENV;

if (hostname.indexOf("mobileweb-w0") != -1) {
  hostTag = "master";
} else if (hostname.indexOf("mobileweb-ws01") != -1) {
  hostTag = "ws";
} else if (hostname.indexOf("mobileweb-t01") != -1) {
  hostTag = "test";
}

const getHpAPI = () => {
  switch (hostTag) {
    case "master":
      return "http://hpapi.eztravel.com.tw/";
    case "ws":
      return "http://hpapi-ws01.eztravel.com.tw/";
    case "test":
      return "http://hpapi-t01.eztravel.com.tw/";
    default:
      return "http://hpapi-t01.eztravel.com.tw/";
  }
};

const getHost = () => {
  switch (hostTag) {
    case "master":
      return "https://static.cdn-eztravel.com.tw/flight/";
    case "ws":
      return "https://static-ws.cdn-eztravel.com.tw/flight/";
    case "test":
      return "https://static-test.cdn-eztravel.com.tw/flight/";
    default:
      return "/";
  }
};

const getVisaAPI = () => {
  switch (hostTag) {
    case "master":
      return "http://ftt-t01.eztravel.com.tw/";
    case "ws":
      return "http://ftt-t01.eztravel.com.tw/";
    case "test":
      return "http://ftt-t01.eztravel.com.tw:8083/";
    default:
      return "http://ftt-t01.eztravel.com.tw:8083/";
  }
};

module.exports = { getHpAPI, getHost, getVisaAPI };
