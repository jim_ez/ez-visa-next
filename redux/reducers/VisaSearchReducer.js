import * as actionTypes from "../constants";

export const INITIAL_STATE = {
  selectedTab: 0,
  loading: true,
  passport: [],
  passportLoading: true,
  countrys: [],
  selectedCountry: "",
  listLoading: true,
  visaList: [],
  detailLoading: true,
  visaDetail: [],
};

function Reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case actionTypes.CHANGE_VISA_TAB:
      return {
        ...state,
        selectedTab: action.no,
      };
    case actionTypes.FETCH_PASSPORT:
      return {
        ...state,
        passportLoading: true,
      };
    case actionTypes.FETCH_PASSPORT_SUCCESS:
      return {
        ...state,
        passportLoading: false,
        passport: action.data,
      };
    case actionTypes.FETCH_PASSPORT_FAIL:
      return {
        ...state,
        passportLoading: false,
      };
    case actionTypes.FETCH_VISACOUNTRY:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.FETCH_VISACOUNTRY_SUCCESS:
      return {
        ...state,
        loading: false,
        countrys: action.data,
      };
    case actionTypes.FETCH_VISACOUNTRY_FAIL:
      return {
        ...state,
        loading: false,
      };
    case actionTypes.FETCH_VISALIST:
      return {
        ...state,
        listLoading: true,
        selectedCountry: action.countryCd,
      };
    case actionTypes.FETCH_VISALIST_SUCCESS:
      return {
        ...state,
        listLoading: false,
        visaList: action.data,
      };
    case actionTypes.FETCH_VISALIST_FAIL:
      return {
        ...state,
        listLoading: false,
      };
    case actionTypes.FETCH_VISADETAIL:
      return {
        ...state,
        detailLoading: true,
      };
    case actionTypes.FETCH_VISADETAIL_SUCCESS:
      return {
        ...state,
        detailLoading: false,
        visaDetail: action.data,
      };
    case actionTypes.FETCH_VISADETAIL_FAIL:
      return {
        ...state,
        detailLoading: false,
      };
  }
  return state;
}

export default Reducer;
