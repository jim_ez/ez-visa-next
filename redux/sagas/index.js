import { all, takeLatest } from "redux-saga/effects";
import * as actionTypes from "../constants";

import * as VisaFreeSaga from "./VisaFreeSaga";
import * as VisaSearchSaga from "./VisaSearchSaga";

function* rootSaga() {
  yield all([
    // visaFree
    takeLatest(actionTypes.FETCH_VISAFREE, VisaFreeSaga.fetchVisaFree),
    // LineCountry
    takeLatest(actionTypes.FETCH_PASSPORT, VisaSearchSaga.fetchPassport),
    takeLatest(actionTypes.FETCH_VISACOUNTRY, VisaSearchSaga.fetchVisaCountry),
    takeLatest(actionTypes.FETCH_VISALIST, VisaSearchSaga.fetchVisaList),
    takeLatest(actionTypes.FETCH_VISADETAIL, VisaSearchSaga.fetchVisaDetail),
  ]);
}

export default rootSaga;
