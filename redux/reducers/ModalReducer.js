import * as actionTypes from "../constants";

export const INITIAL_STATE = {
  open: false,
};

function Reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case actionTypes.OPEN_MODAL:
      return {
        ...state,
        open: true,
      };
    case actionTypes.CLOSE_MODAL:
      return {
        ...state,
        open: false,
      };
  }
  return state;
}

export default Reducer;
