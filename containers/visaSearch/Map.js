import React from "react";
import styles from "./Map.module.scss";

import { Stage, Layer, Group, Rect, Text, Image } from "react-konva";
import { Spring, animated } from "react-spring/renderprops-konva.cjs";

import { position } from "./mapPosition";

import { useSelector, useDispatch } from "react-redux";
import * as VisaSearchActions from "../../redux/actions/VisaSearchAction";

const Shadow = () => {
  return (
    <Spring
      native
      from={{ x: 0, y: 0, width: 0, height: 0, shadowBlur: 0 }}
      to={{
        shadowBlur: 5,
        fill: "rgba(160,0,0,0.2)",
        width: 80,
        height: 80,
      }}
    >
      {(props) => <animated.Circle {...props} />}
    </Spring>
  );
};

const Line = ({ start, point }) => {
  let x_center = 20;
  let sx = 0 - x_center;
  let sy = 0;
  let t1x = (point.x - start.x) / 2 - x_center;
  let t1y = (point.y - start.y) / 2 - point.bezier * 10;
  let ex = point.x - start.x - x_center;
  let ey = point.y - start.y;
  return (
    <Spring
      native
      from={{
        x: 18,
        y: 18,
        points: [sx, sy, sx, sy, sx, sy, sx, sy],
        stroke: "rgba(0,0,0,0.2)",
        dash: [4, 2],
      }}
      to={{
        points: [sx, sy, t1x, t1y, t1x, t1y, ex, ey],
      }}
    >
      {(props) => <animated.Line {...props} bezier />}
    </Spring>
  );
};

const AllLine = ({ position }) => {
  return position.map(
    (item) =>
      item.countryCd != "TW" && <Line start={position[0]} point={item} />
  );
};

const Map = () => {
  const dispatch = useDispatch();
  const { selectedTab, countrys, selectedCountry } = useSelector(
    (state) => state.VisaSearchReducer
  );
  const showCountrys = position.filter((position) =>
    // 只需顯示API回傳的城市(同時要定義過mapPosition)
    countrys.find((country) => country.countryCd == position.countryCd)
  );
  const changeLineCountrySelected = (countryCd) => {
    if (countryCd == "TW") {
      dispatch(VisaSearchActions.changeVisaTab(0));
    } else {
      dispatch(VisaSearchActions.changeVisaTab(1));
    }
    dispatch(VisaSearchActions.fetchVisaList(countryCd));
  };
  return (
    <div>
      <Stage width={816} height={345}>
        <Layer>
          {showCountrys.map((item, i) => (
            <Group
              key={i}
              x={item.x}
              y={item.y}
              onMouseDown={() => changeLineCountrySelected(item.countryCd)}
              onMouseEnter={(e) => {
                // style stage container:
                const container = e.target.getStage().container();
                container.style.cursor = "pointer";
              }}
              onMouseLeave={(e) => {
                const container = e.target.getStage().container();
                container.style.cursor = "default";
              }}
            >
              <Text
                width={40}
                height={40}
                x={-20}
                y={-20}
                align="center"
                verticalAlign="middle"
                padding={5}
                text={item.countryCd}
              />
              {selectedTab == 0 && item.countryCd == "TW" && (
                <AllLine position={showCountrys} />
              )}
              {selectedTab == 1 && item.countryCd == selectedCountry && (
                <Shadow />
              )}
            </Group>
          ))}
        </Layer>
      </Stage>
    </div>
  );
};

export default Map;
