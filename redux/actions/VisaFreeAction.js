import * as actionTypes from "../constants";

export const fetchVisaFree = () => ({
  type: actionTypes.FETCH_VISAFREE,
});

export const fetchVisaFreeScs = (data) => ({
  type: actionTypes.FETCH_VISAFREE_SUCCESS,
  data,
});

export const fetchVisaFreeFail = () => ({
  type: actionTypes.FETCH_VISAFREE_FAIL,
});

export const showVisaFreeDetail = (no) => ({
  type: actionTypes.SHOW_VISAFREE_DETAIL,
  no,
});
