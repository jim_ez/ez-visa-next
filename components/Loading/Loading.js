import React from "react";
import PropTypes from "prop-types";
import CircularProgress from "@material-ui/core/CircularProgress";

import styles from "./Loading.module.scss";

const Loading = ({ loading, children }) => {
  return (
    <>
      {loading ? (
        <div className={styles.wrapper}>
          <CircularProgress />
        </div>
      ) : (
        children
      )}
    </>
  );
};

Loading.propTypes = {
  loading: PropTypes.bool,
};

Loading.defaultProps = {
  loading: false,
};

export default Loading;
