import React from "react";
import Meta from "../components/Meta/Meta";
import "../styles/global.scss";

const pageLayout = origin => Page => {
  class PageLayout extends React.Component {
    static async getInitialProps(context) {
      const title = "test";
      return {
        title
      };
    }
    render() {
      const { title } = this.props;
      return (
        <>
          <Meta />
          <main>
            <h1>title {title}</h1>
            <Page />
          </main>
        </>
      );
    }
  }
  return PageLayout;
};

export default pageLayout;
