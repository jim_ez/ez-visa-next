import React from "react";
import PropTypes from "prop-types";
import styles from "./ModalDetail.module.scss";

const VisaFreeDetail = ({ detailInfo }) => {
  return detailInfo ? (
    <div>
      <div className={styles.columnBox}>
        <div className={styles.title}>免簽證國家</div>
        <div className={styles.content}>{detailInfo.countryChn}</div>
      </div>
      <div className={styles.columnBox}>
        <div className={styles.title}>備註</div>
        <div className={styles.content}>
          <div dangerouslySetInnerHTML={{ __html: detailInfo.descs }}></div>
        </div>
      </div>
    </div>
  ) : (
    <div>查無資料，請稍候在試</div>
  );
};

VisaFreeDetail.propTypes = {
  detailInfo: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

VisaFreeDetail.defaultProps = {
  detailInfo: false,
};

export default VisaFreeDetail;
