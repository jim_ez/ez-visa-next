import React from "react";
import PropTypes from "prop-types";
import styles from "./GuideRow.module.scss";

const GuideRow = ({ title }) => {
  return (
    <div className={styles.guideRow}>
      <a href="//www.eztravel.com.tw" className={styles.homeIcon}></a>
      <span>></span>
      <a href="//lcc.eztravel.com.tw" className={styles.pageStyle}>
        護照、簽證
      </a>
      <span>></span>
      <a className={styles.currentlyPage}>{title}</a>
    </div>
  );
};

GuideRow.propTypes = {
  step: PropTypes.string,
};

GuideRow.defaultProps = {
  step: "",
};

export default GuideRow;
