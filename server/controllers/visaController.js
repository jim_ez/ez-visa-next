const { Visa } = require("../../utils/uri.server");
const axios = require("axios");

exports.getVisaFree = async (ctx, next) => {
  ctx.status = 200;
  try {
    let data = await axios
      .get(Visa.getVisaFree)
      .then(function (response) {
        return response.data.items;
      })
      .catch((err) => {
        console.log("err: ", err);
        return null;
      });
    ctx.response.body = data;
  } catch (error) {
    console.log("error: ", error);
    ctx.throw(500, "network fetch error");
    next();
  }
};

exports.getVisaOnArrival = async (ctx, next) => {
  ctx.status = 200;
  try {
    let data = await axios
      .get(Visa.getVisaFree)
      .then(function (response) {
        return response.data.items;
      })
      .catch((err) => {
        console.log("err: ", err);
        return null;
      });
    ctx.response.body = data;
  } catch (error) {
    console.log("error: ", error);
    ctx.throw(500, "network fetch error");
    next();
  }
};

exports.getPassport = async (ctx, next) => {
  ctx.status = 200;
  try {
    let data = await axios
      .get(Visa.getPassport)
      .then(function (response) {
        return response.data.items;
      })
      .catch((err) => {
        console.log("err: ", err);
        return null;
      });
    ctx.response.body = data;
  } catch (error) {
    console.log("error: ", error);
    ctx.throw(500, "network fetch error");
    next();
  }
};

exports.getVisaCountry = async (ctx, next) => {
  ctx.status = 200;
  try {
    let data = await axios
      .get(Visa.getVisaCountry)
      .then(function (response) {
        return response.data.items;
      })
      .catch((err) => {
        console.log("err: ", err);
        return null;
      });
    ctx.response.body = data;
  } catch (error) {
    console.log("error: ", error);
    ctx.throw(500, "network fetch error");
    next();
  }
};

exports.getVisaList = async (ctx, next) => {
  ctx.status = 200;
  try {
    const { countryCode } = ctx.request.body;
    let data = await axios
      .get(Visa.getVisaList(countryCode))
      .then(function (response) {
        return response.data.items;
      })
      .catch((err) => {
        console.log("err: ", err);
        return null;
      });
    ctx.response.body = data;
  } catch (error) {
    console.log("error: ", error);
    ctx.throw(500, "network fetch error");
    next();
  }
};

exports.getVisaDetail = async (ctx, next) => {
  ctx.status = 200;
  try {
    const { prodNo, docType } = ctx.request.body;
    let data = await axios
      .get(Visa.getVisaDetail(prodNo, docType))
      .then(function (response) {
        return response.data.items;
      })
      .catch((err) => {
        console.log("err: ", err);
        return null;
      });
    ctx.response.body = data;
  } catch (error) {
    console.log("error: ", error);
    ctx.throw(500, "network fetch error");
    next();
  }
};
