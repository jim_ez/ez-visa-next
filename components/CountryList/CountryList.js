import React from "react";
import PropTypes from "prop-types";
import styles from "./CountryList.module.scss";

const lineChnPriority = [
  "東北亞",
  "東南亞",
  "港澳大陸",
  "美加",
  "紐澳",
  "歐洲",
  "非洲",
  "中南美洲",
  "太平洋島嶼",
  "西亞",
];

const LineBlock = ({ title, countrys, showDetail }) => {
  return (
    <div className={styles.block}>
      <div className={styles.blockTitle}>{title}</div>
      <div className={styles.blockContent}>
        {countrys.map((datas, index) => (
          <div
            key={`visaFree_${title}_${index}`}
            className={styles.countryList}
            onClick={() => showDetail(datas.prodNo)}
          >
            <div className={styles.title}>{datas.countryChn}</div>
            <div className={styles.days}>可停留 {datas.enterQty} 天</div>
            <div className={styles.more}>詳細內容</div>
          </div>
        ))}
      </div>
    </div>
  );
};

const CountryList = ({ info, showDetail }) => {
  const newArray = lineChnPriority.map((title) => {
    const countrys = info.filter(function (item, index, array) {
      return item.lineChn == title;
    });
    return {
      title: title,
      countrys: countrys,
    };
  });
  return (
    <>
      {newArray.map((data, index) => (
        <LineBlock
          key={`lineBlock_${index}`}
          title={data.title}
          countrys={data.countrys}
          showDetail={showDetail}
        />
      ))}
    </>
  );
};

CountryList.propTypes = {
  info: PropTypes.array,
};

CountryList.defaultProps = {
  info: "",
};

export default CountryList;
