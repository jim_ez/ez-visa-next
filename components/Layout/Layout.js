import React from "react";
import Meta from "../Meta/Meta";
import styles from "./Layout.module.scss";

class Layout extends React.Component {
  render() {
    const { children, meta } = this.props;
    return (
      <>
        <Meta {...meta} />
        <main className={styles.main}>{children}</main>
      </>
    );
  }
}

export default Layout;
