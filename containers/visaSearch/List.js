import React from "react";
import styles from "./List.module.scss";

import { useSelector, useDispatch } from "react-redux";
import * as VisaSearchActions from "../../redux/actions/VisaSearchAction";
import * as ModalActions from "../../redux/actions/ModalAction";

import { useSpring, animated } from "react-spring";

const AnimateBox = ({ children }) => {
  const props = useSpring({
    opacity: 1,
    transform: "translate3d(0,0,0)",
    from: { opacity: 0, transform: "translate3d(-40px,0,0)" },
    config: { duration: 250 },
  });
  return (
    <animated.div className={styles.block} style={props}>
      {children}
    </animated.div>
  );
};

const PassportBlock = ({ title }) => {
  return <AnimateBox>{title}</AnimateBox>;
};

const ListBlock = ({ title, showDetail }) => {
  return (
    <AnimateBox>
      {title}
      <span className={styles.showDetail} onClick={showDetail}>
        more
      </span>
    </AnimateBox>
  );
};

const List = () => {
  const dispatch = useDispatch();
  const { selectedTab, passport, visaList } = useSelector(
    (state) => state.VisaSearchReducer
  );
  const showDetail = (prodNo, docType) => {
    dispatch(VisaSearchActions.fetchVisaDetail(prodNo, docType));
    dispatch(ModalActions.openModal());
  };
  return (
    <div className={styles.bg}>
      {selectedTab == 0 ? (
        passport.length > 0 ? (
          passport.map((data, index) => (
            <PassportBlock
              key={`passportBlock_${index}`}
              title={data.prodName}
              showDetail={() => showDetail(data.prodNo, data.docType)}
            />
          ))
        ) : (
          <p>查無資料</p>
        )
      ) : visaList.length > 0 ? (
        visaList.map((data, index) => (
          <ListBlock
            key={`visaListBlock_${index}`}
            title={data.prodName}
            showDetail={() => showDetail(data.prodNo, data.docType)}
          />
        ))
      ) : (
        <p>查無資料</p>
      )}
    </div>
  );
};

export default List;
