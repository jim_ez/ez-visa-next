import React from "react";
import { useDispatch } from "react-redux";
import styles from "./List.module.scss";

import GuideRow from "../../components/GuideRow/GuideRow";
import Loading from "../../components/Loading/Loading";
import CountryListComponent from "../../components/CountryList/CountryList";

import * as ModalActions from "../../redux/actions/ModalAction";
import * as VisaFreeActions from "../../redux/actions/VisaFreeAction";

const CountryList = ({ visaFreeInfo, loading }) => {
  const title = "免簽證國家";
  const dispatch = useDispatch();
  const showDetail = (prodNo) => {
    dispatch(ModalActions.openModal());
    dispatch(VisaFreeActions.showVisaFreeDetail(prodNo));
  };
  return (
    <div className={styles.bg}>
      <div className="container">
        <GuideRow title={title} />
        <h1 className={styles.title}>{title}</h1>
        <div className={styles.des}>
          <h3>免簽證須知</h3>
          <p>
            各國簽證及移民法規時有變動，請於出發前向有關國家駐海外使領館或代表機構確認，相關入境規定仍以入境國政府最新公布者為準。
          </p>
        </div>
        <div className={styles.wrapper}>
          <Loading loading={loading}>
            {visaFreeInfo.length > 1 ? (
              <CountryListComponent
                info={visaFreeInfo}
                showDetail={showDetail}
              />
            ) : (
              <p>查無資料</p>
            )}
          </Loading>
        </div>
      </div>
    </div>
  );
};

export default CountryList;
