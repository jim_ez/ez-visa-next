import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Layout from "../components/Layout/Layout";
import VisaSearchList from "../containers/visaSearch/Index";
import Loading from "../components/Loading/Loading";
import Modal from "../components/Modal/Modal";
import ModalContent from "../containers/visaSearch/ModalDetail";

import * as ModalActions from "../redux/actions/ModalAction";
import * as VisaSearchActions from "../redux/actions/VisaSearchAction";

function VisaSearch() {
  const dispatch = useDispatch();
  const { open } = useSelector((state) => state.ModalReducer);
  const { visaDetail, detailLoading } = useSelector(
    (state) => state.VisaSearchReducer
  );
  useEffect(() => {
    fetchVisaCountry();
    fetchPassport();
  }, []);
  const fetchVisaCountry = async () => {
    dispatch(VisaSearchActions.fetchVisaCountry());
  };
  const fetchPassport = async () => {
    dispatch(VisaSearchActions.fetchPassport());
  };
  const handleClose = () => {
    dispatch(ModalActions.closeModal());
  };
  return (
    <Layout>
      <VisaSearchList />
      <Modal
        open={open}
        onClose={handleClose}
        title="護照辦理查詢"
        content={
          <Loading loading={detailLoading}>
            <ModalContent detailInfo={visaDetail} />
          </Loading>
        }
      />
    </Layout>
  );
}

export default VisaSearch;
