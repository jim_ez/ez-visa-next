import Layout from "../components/Layout/Layout";

function Index({ title }) {
  return (
    <Layout>
      <div>{title}</div>
      <ul>
        <li>
          <a href="/visaFree">visaFree</a>
        </li>
        <li>
          <a href="/visaSearch">visaSearch</a>
        </li>
      </ul>
    </Layout>
  );
}

export async function getServerSideProps() {
  const title = "title2";
  return {
    props: {
      title,
    },
  };
}

export default Index;
