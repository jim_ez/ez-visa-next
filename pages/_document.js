import Document, { Head, Main, NextScript } from "next/document";
import { Ez as EzUri } from "../utils/uri.server";
import { fetchApi, clearScriptTag, getScriptTag } from "../utils/tool";

const ShowHtml = ({ html }) => {
  return <div dangerouslySetInnerHTML={{ __html: html }} />;
};

export default class extends Document {
  static async getInitialProps(ctx) {
    const headerApi =
      ctx.query.ezHeaderFooter == "simple"
        ? EzUri.getEzSpHeader
        : EzUri.getEzHeader;
    const jsCssApi =
      ctx.query.ezHeaderFooter == "simple"
        ? EzUri.getEzSpJsCss
        : EzUri.getEzJsCss;
    const footerApi =
      ctx.query.ezHeaderFooter == "simple"
        ? EzUri.getEzSpFooter
        : EzUri.getEzFooter;
    const getHeaderHtml = await fetchApi(headerApi);
    const headerHtml = getHeaderHtml ? clearScriptTag(getHeaderHtml) : null;
    const headerScriptSrc = getHeaderHtml ? getScriptTag(getHeaderHtml) : null;
    const jsCssHtml = await fetchApi(jsCssApi);
    const footerHtml = await fetchApi(footerApi);
    const initialProps = await Document.getInitialProps(ctx);
    return {
      headerHtml,
      headerScriptSrc,
      jsCssHtml,
      footerHtml,
      ...initialProps,
    };
  }
  render() {
    const { headerHtml, headerScriptSrc, jsCssHtml, footerHtml } = this.props;
    return (
      <html lang="zh-Hant-TW">
        <Head />
        <body>
          {headerHtml && <ShowHtml html={headerHtml} />}
          {jsCssHtml && <ShowHtml html={jsCssHtml} />}
          <Main />
          {footerHtml && <ShowHtml html={footerHtml} />}
          {headerScriptSrc && <ShowHtml html={headerScriptSrc} />}
          <NextScript />
        </body>
      </html>
    );
  }
}
