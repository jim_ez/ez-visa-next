import * as actionTypes from "../constants";

export const changeVisaTab = (no) => ({
  type: actionTypes.CHANGE_VISA_TAB,
  no,
});

export const fetchPassport = () => ({
  type: actionTypes.FETCH_PASSPORT,
});

export const fetchPassportScs = (data) => ({
  type: actionTypes.FETCH_PASSPORT_SUCCESS,
  data,
});

export const fetchPassportFail = () => ({
  type: actionTypes.FETCH_PASSPORT_FAIL,
});

export const fetchVisaCountry = () => ({
  type: actionTypes.FETCH_VISACOUNTRY,
});

export const fetchVisaCountryScs = (data) => ({
  type: actionTypes.FETCH_VISACOUNTRY_SUCCESS,
  data,
});

export const fetchVisaCountryFail = () => ({
  type: actionTypes.FETCH_VISACOUNTRY_FAIL,
});

export const fetchVisaList = (countryCd) => ({
  type: actionTypes.FETCH_VISALIST,
  countryCd,
});

export const fetchVisaListScs = (data) => ({
  type: actionTypes.FETCH_VISALIST_SUCCESS,
  data,
});

export const fetchVisaListFail = () => ({
  type: actionTypes.FETCH_VISALIST_FAIL,
});

export const fetchVisaDetail = (prodNo, docType) => ({
  type: actionTypes.FETCH_VISADETAIL,
  prodNo,
  docType,
});

export const fetchVisaDetailScs = (data) => ({
  type: actionTypes.FETCH_VISADETAIL_SUCCESS,
  data,
});

export const fetchVisaDetailFail = () => ({
  type: actionTypes.FETCH_VISADETAIL_FAIL,
});
