import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Layout from "../components/Layout/Layout";
import VisaFreeList from "../containers/visaFree/List";

import Modal from "../components/Modal/Modal";
import ModalContent from "../containers/visaFree/ModalDetail";

import * as ModalActions from "../redux/actions/ModalAction";
import * as VisaFreeActions from "../redux/actions/VisaFreeAction";

function VisaFree() {
  const dispatch = useDispatch();
  const { lists, loading, selected } = useSelector(
    (state) => state.VisaFreeReducer
  );
  const { open } = useSelector((state) => state.ModalReducer);
  useEffect(() => {
    fetchVisaFree();
  }, []);
  const fetchVisaFree = async () => {
    dispatch(VisaFreeActions.fetchVisaFree());
  };
  const handleClose = () => {
    dispatch(ModalActions.closeModal());
  };
  return (
    <Layout>
      <VisaFreeList visaFreeInfo={lists} loading={loading} />
      <Modal
        open={open}
        onClose={handleClose}
        title="護照辦理查詢"
        content={
          <ModalContent
            detailInfo={
              selected
                ? lists.find((element) => element.prodNo == selected)
                : false
            }
          />
        }
      />
    </Layout>
  );
}

export default VisaFree;
