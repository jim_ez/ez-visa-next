import React from "react";
import styles from "./CountryTab.module.scss";

import { useSelector, useDispatch } from "react-redux";
import * as VisaSearchActions from "../../redux/actions/VisaSearchAction";

const CountryTab = () => {
  const dispatch = useDispatch();
  const { countrys, selectedCountry } = useSelector(
    (state) => state.VisaSearchReducer
  );
  const changeLineCountrySelected = (countryCd) => {
    dispatch(VisaSearchActions.fetchVisaList(countryCd));
  };
  // 處理tab呈現資料
  let TabArray = [];
  countrys.forEach((datas, index) => {
    let lineChn = datas.lineChn;
    let arrayIndex = TabArray.findIndex((element) => element.title == lineChn);
    if (arrayIndex !== -1) {
      TabArray[arrayIndex].countrys.push({
        countryChn: datas.countryChn,
        countryCd: datas.countryCd,
      });
    } else {
      TabArray.push({
        title: lineChn,
        countrys: [
          {
            countryChn: datas.countryChn,
            countryCd: datas.countryCd,
          },
        ],
      });
    }
  });
  return (
    <>
      <div className={styles.title}>
        目的地
        <div>
          <a href="/visaFree">免簽證</a>
          <a href="#">落地簽證</a>
        </div>
      </div>
      <div className={styles.content}>
        <ul className={styles.nav}>
          {TabArray.map((datas, index) => (
            <li
              key={`line_${index}`}
              className={
                datas.countrys.findIndex(
                  (element) => element.countryCd == selectedCountry
                ) != -1
                  ? styles.active
                  : ""
              }
            >
              <span
                onClick={() =>
                  changeLineCountrySelected(datas.countrys[0].countryCd)
                }
              >
                {datas.title}
              </span>
              <SubNav
                countrys={datas.countrys}
                selectedCountry={selectedCountry}
                changeSelected={changeLineCountrySelected}
              />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

const SubNav = ({ countrys, selectedCountry, changeSelected }) => {
  return (
    <ul className={styles.subNav}>
      {countrys.map((datas, index) => (
        <li
          key={`subnav_${datas.countryCd}_${index}`}
          className={datas.countryCd == selectedCountry ? styles.active : ""}
        >
          <span onClick={() => changeSelected(datas.countryCd)}>
            {datas.countryChn}
          </span>
        </li>
      ))}
    </ul>
  );
};

export default CountryTab;
