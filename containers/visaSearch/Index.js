import React from "react";
import styles from "./Index.module.scss";

import SearchTab from "./SearchTab";
import Map from "./Map";
import List from "./List";

const CountryList = ({ countrys }) => {
  const title = "護照、簽證";
  return (
    <div className={styles.bg}>
      <div className="container">
        <h1 className={styles.title}>{title}</h1>
        <div className={styles.menuWrapper}>
          <div className={styles.tabWrapper}>
            <SearchTab />
          </div>
          <div className={styles.mapWrapper}>
            <Map />
          </div>
        </div>
        <h3 className={styles.title}>相關證件</h3>
        <div className={styles.listWrapper}>
          <List />
        </div>
      </div>
    </div>
  );
};

export default CountryList;
