const position = [
  //第一筆為起始點
  {
    countryCd: "TW",
    x: 400,
    y: 120,
  },
  {
    countryCd: "CN",
    x: 200,
    y: 50,
    bezier: 0,
  },
  {
    countryCd: "NZ",
    x: 500,
    y: 180,
    bezier: 0,
  },
  {
    countryCd: "NO",
    x: 600,
    y: 60,
    bezier: 0,
  },
];

module.exports = { position };
