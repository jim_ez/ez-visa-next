const { getHpAPI, getVisaAPI } = require("../utils/config.server.js");

const Ez = {
  getEzHeader: `${getHpAPI()}v1/api/ezHeader`,
  getEzSpHeader: `${getHpAPI()}v1/api/ezSpHeader`,
  getEzFooter: `${getHpAPI()}v1/api/ezFooter`,
  getEzSpFooter: `${getHpAPI()}v1/api/ezSpFooter`,
  getEzJsCss: `${getHpAPI()}v2/api/ezJsCss`,
  getEzSpJsCss: `${getHpAPI()}v2/api/ezSpJsCss`,
};

const Visa = {
  getVisaFree: `${getVisaAPI()}visa/rest/v1/visa-free`,
  getVisaOnArrival: `${getVisaAPI()}visa/rest/v1/visa-on-arrival`,
  getPassport: `${getVisaAPI()}visa/rest/v1/passport`,
  getVisaCountry: `${getVisaAPI()}visa/rest/v1/visa/line-countries`,
  getVisaList: (countryCode) =>
    `${getVisaAPI()}visa/rest/v1/visa/${countryCode}`,
  getVisaDetail: (prodNo, docType) =>
    `${getVisaAPI()}visa/rest/v1/visa/${prodNo}/${docType}`,
};

module.exports = { Ez, Visa };
