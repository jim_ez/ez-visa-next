import React, { useEffect } from "react";
import PropTypes from "prop-types";
import styles from "./Modal.module.scss";

import Modal from "@material-ui/core/Modal";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const EzModal = ({ open, onClose, title, content }) => {
  const [modalStyle] = React.useState(getModalStyle);
  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <div style={modalStyle} className={styles.wrapper}>
        <div className={styles.title}>
          {title}
          <span onClick={onClose} className={styles.closeBtn}>
            X
          </span>
        </div>
        <div className={styles.content}>
          <div className={styles.scrollBox}>{content}</div>
        </div>
      </div>
    </Modal>
  );
};

EzModal.propTypes = {
  open: PropTypes.bool,
};

EzModal.defaultProps = {
  open: false,
};

export default EzModal;
