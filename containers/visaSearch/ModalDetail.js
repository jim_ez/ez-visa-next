import React from "react";
import styles from "./ModalDetail.module.scss";

const VisaDetail = ({ detailInfo }) => {
  return detailInfo ? (
    <div>
      <div className={styles.columnBox}>
        <div className={styles.title}>免簽證國家</div>
        <div className={styles.content}>{detailInfo.countryChn}</div>
      </div>
      <div className={styles.columnBox}>
        <div className={styles.title}>類型</div>
        <div className={styles.content}>{detailInfo.docDesc}</div>
      </div>
      <div className={styles.columnBox}>
        <div className={styles.title}>備註</div>
        <div className={styles.content}>
          <div dangerouslySetInnerHTML={{ __html: detailInfo.descs }}></div>
        </div>
      </div>
    </div>
  ) : (
    <div>查無資料，請稍候在試</div>
  );
};

export default VisaDetail;
