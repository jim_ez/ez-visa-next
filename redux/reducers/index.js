import { combineReducers } from "redux";
import ModalReducer from "./ModalReducer";
import VisaFreeReducer from "./VisaFreeReducer";
import VisaSearchReducer from "./VisaSearchReducer";

export default combineReducers({
  ModalReducer,
  VisaFreeReducer,
  VisaSearchReducer,
});
