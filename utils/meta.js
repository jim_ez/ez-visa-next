export const home = {
  title: "ezTravel易遊網 | 易遊世界．觸手可及",
  description:
    "國內外團體旅遊行程推薦、機加酒自由行、便宜機票訂位、飯店住宿訂房優惠、全球遊輪、火車高鐵線上預訂、玩樂票券折扣與護照簽證代辦，全網售價無可挑戰",
  keywords: "機票,旅遊,訂房,自由行,旅行社",
  ogtitle: "ezTravel易遊網 | 易遊世界．觸手可及",
  ogsitename: "ezTravel易遊網",
  ogtype: "website",
  ogdescription:
    "國內外團體旅遊行程推薦、機加酒自由行、便宜機票訂位、飯店住宿訂房優惠、全球遊輪、火車高鐵線上預訂、玩樂票券折扣與護照簽證代辦，全網售價無可挑戰",
  ogimage: "https://www.eztravel.com.tw/hpimg/og/homepage.png",
  ogurl: "https://www.eztravel.com.tw/",
  canonical: "https://www.eztravel.com.tw"
};
