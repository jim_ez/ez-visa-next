import React from "react";
import styles from "./SearchTab.module.scss";

import PassportTab from "./PassportTab";
import CountryTab from "./CountryTab";

import { useSelector, useDispatch } from "react-redux";
import * as VisaSearchActions from "../../redux/actions/VisaSearchAction";

const EzTab = () => {
  const dispatch = useDispatch();
  const { selectedTab } = useSelector((state) => state.VisaSearchReducer);
  const setTab = (index) => {
    dispatch(VisaSearchActions.changeVisaTab(index));
  };
  const title = ["護照申辦", "各國簽證", "國際學生證"];
  const content = [<PassportTab />, <CountryTab />];
  return (
    <>
      <div className={styles.tabWrapper}>
        {title.map((val, index) => (
          <div
            key={`visaTab${index}`}
            className={`${styles.tab} ${selectedTab == index && styles.active}`}
            onClick={() => {
              if (index == 2) {
                window.location.href = "/visaFree";
              } else {
                setTab(index);
              }
            }}
          >
            {val}
          </div>
        ))}
      </div>
      <div className={styles.tabContentWrapper}>
        {content.map((val, index) => (
          <div
            key={`visaTabContent${index}`}
            className={`${styles.content} ${
              selectedTab == index && styles.active
            }`}
          >
            {val}
          </div>
        ))}
      </div>
    </>
  );
};

export default EzTab;
