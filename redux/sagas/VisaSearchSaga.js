import { call, put } from "redux-saga/effects";
import axios from "axios";
import * as VisaSearchAction from "../actions/VisaSearchAction";

export function* fetchPassport() {
  try {
    const response = yield call(axios.get, "/api/getPassport");
    if (response.status === 200) {
      yield put(VisaSearchAction.fetchPassportScs(response.data));
      return;
    } else {
      yield put(VisaSearchAction.fetchPassportFail());
      return;
    }
  } catch (error) {
    console.log(error);
  }
}

export function* fetchVisaCountry() {
  try {
    const response = yield call(axios.get, "/api/getVisaCountry");
    if (response.status === 200) {
      let lineCountry = response.data;
      lineCountry.push({
        countryCd: "HK",
        countryChn: "香港",
        lineCd: "204",
        lineChn: "港澳大陸",
      });
      yield put(VisaSearchAction.fetchVisaCountryScs(response.data));
      return;
    } else {
      yield put(VisaSearchAction.fetchVisaCountryFail());
      return;
    }
  } catch (error) {
    console.log(error);
  }
}

export function* fetchVisaList(action) {
  try {
    const params = {
      countryCode: action.countryCd,
    };
    const response = yield call(axios.post, "/api/getVisaList", params);
    if (response.status === 200) {
      yield put(VisaSearchAction.fetchVisaListScs(response.data));
      return;
    } else {
      yield put(VisaSearchAction.fetchVisaListFail());
      return;
    }
  } catch (error) {
    console.log(error);
  }
}

export function* fetchVisaDetail(action) {
  try {
    const params = {
      prodNo: action.prodNo,
      docType: action.docType,
    };
    const response = yield call(axios.post, "/api/getVisaDetail", params);
    if (response.status === 200) {
      yield put(VisaSearchAction.fetchVisaDetailScs(response.data[0]));
      return;
    } else {
      yield put(VisaSearchAction.fetchVisaDetailFail());
      return;
    }
  } catch (error) {
    console.log(error);
  }
}
