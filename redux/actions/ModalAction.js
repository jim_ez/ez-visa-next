import * as actionTypes from "../constants";

export const openModal = () => ({
  type: actionTypes.OPEN_MODAL,
});

export const closeModal = (data) => ({
  type: actionTypes.CLOSE_MODAL,
});
