import { call, put } from "redux-saga/effects";
import axios from "axios";
import * as VisaFreeAction from "../actions/VisaFreeAction";

// fetchVisaFree
export function* fetchVisaFree() {
  try {
    const response = yield call(axios.get, "/api/getVisaFree");
    if (response.status === 200) {
      yield put(VisaFreeAction.fetchVisaFreeScs(response.data));
      return;
    } else {
      yield put(VisaFreeAction.fetchVisaFreeFail());
      return;
    }
  } catch (error) {
    console.log(error);
  }
}
