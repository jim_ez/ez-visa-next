import React from 'react'
import Head from 'next/head'
import { home as meta } from '../../utils/meta'

const Meta = ({
  title = meta.title,
  description = meta.description,
  keywords = meta.keywords,
  ogtitle = meta.ogtitle,
  ogdescription = meta.ogdescription,
  ogtype = meta.ogtype,
  ogimage = meta.ogimage,
  ogsitename = meta.ogsitename,
  ogurl = meta.ogurl,
  canonical = meta.canonical
}) => (
  <Head>
    <title>{title}</title>
    <meta charSet="utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <meta name="google-site-verification" content="AIkmj1WfZgZ8rvvCH_TUjQYsoCStXeq41UIOpGU_OO8" />
    <meta name="msvalidate.01" content="4702C7B0B8E5B0274E7B37A5C2230479" />
    <meta name="description" content={description} />
    <meta name="keywords" content={keywords} />
    <meta name="format-detection" content="telephone=no" />
    <meta property="og:title" content={ogtitle} />
    <meta property="og:description" content={ogdescription} />
    <meta property="og:type" content={ogtype} />
    <meta property="og:image" content={ogimage} />
    <meta property="og:site_name" content={ogsitename} />
    {!!ogurl && <meta property="og:url" content={ogurl} />}
    <link rel="shortcut icon" href="https://www.eztravel.com.tw/assets/images/common/logo.jpg" />
    {!!canonical && <link rel="canonical" href={canonical} />}
  </Head>
)

export default Meta
