// next.config.js
const path = require("path");
const webpack = require("webpack");

const withPlugins = require("next-compose-plugins");
const withImageOptimize = require("next-optimized-images");

const { getHost } = require("./utils/config.server.js");

const dev = process.env.NODE_ENV !== "production";
const hostStr = getHost();
const assetPrefix = dev ? "/" : hostStr;
const imgPrefix = dev ? "/static/images/" : `${hostStr}images/`;

const imagePlugin = [
  withImageOptimize,
  {
    imagesOutputPath: "images/",
    imagesPublicPath: imgPrefix,
    imagesName: "[name]-[hash:4].[ext]",
    // optimizeImagesInDev: true,
    inlineImageLimit: -1,
    mozjpeg: {
      quality: 95,
    },
    svgo: {
      plugins: [{ removeComments: true, removeViewBox: true }],
    },
  },
];

const nextConfig = {
  // 設定產出路徑為 /build 取代 /.next (for google cloud storage)
  distDir: "build",
  assetPrefix,
  webpack: (config, { isServer, buildId }) => {
    /**
     * 讓專案可以直接取得 static/images folder 不需要相對路徑
     */
    config.resolve.alias["~"] = path.join(__dirname, "public/static/images");
    if (!isServer) {
      // 取代 @zeit/next-css的 自動打包成一整隻style.css
      config.optimization.splitChunks.cacheGroups.styles = {
        name: "globalStyles",
        test: /styles\/.+\.scss$/,
        chunks: "all",
        enforce: true,
        priority: 2,
      };
    }
    return config;
  },
};

module.exports = withPlugins([imagePlugin], nextConfig);
